import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, wait
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import Select

import ReturnCommon

def eaSearchOrder(driver, parentOrderNumber):
    time.sleep(10)
    wait = WebDriverWait(driver, 4)
    searchText = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="searchOrders"]')))
    searchText.send_keys(parentOrderNumber)
    # searchText.send_keys(Keys.RETURN)
    time.sleep(2)
    driver.find_element_by_xpath(
        '/html/body/div[1]/main/div/div/div[2]/div/div[14]/div/div[2]/table/tbody/tr[1]/td[1]/button').click()


def eaApproveRefund(driver):
    rows = driver.find_elements_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[14]/div/div[2]/div[1]/div[2]/div')
    rowsLength = len(rows)
    i = 2
    while rowsLength >= i :
        fullXpath = '/html/body/div[1]/main/div/div/div[2]/div/div[14]/div/div[2]/div[1]/div[2]/div['+str(i)+']/div/div/div[1]/div/div/div[6]/div/div/select'
        flag = isElementExist(driver, fullXpath)
        if flag:
            sel = driver.find_element_by_xpath(fullXpath)
            driver.execute_script("arguments[0].scrollIntoView(false)", sel)
            time.sleep(1)
            Select(sel).select_by_value('Full refund')

            time.sleep(1)
        i = i + 1
    time.sleep(1)
    btn = driver.find_element_by_xpath('//div[text()="SUBMIT REFUND DECISION"]')
    driver.execute_script("arguments[0].scrollIntoView(false)", btn)
    time.sleep(1)
    btn.click()

    time.sleep(2)
    btn = driver.find_element_by_xpath('//div[text()="CONFIRM REFUND DECISION"]')
    driver.execute_script("arguments[0].scrollIntoView(false)", btn)
    time.sleep(1)
    btn.click()

def isElementExist(driver, xPath):
    flag=True
    ele=driver.find_elements_by_xpath(xPath)
    if len(ele)==0:
        flag=False
        return flag
    if len(ele)==1:
        return flag
    else:
        flag=False
        return flag

def eaRejectRefund(driver):
    time.sleep(5)
    rows = driver.find_elements_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[14]/div/div[2]/div[1]/div[2]/div')
    rowsLength = len(rows)
    i = 2
    while rowsLength >= i :
        fullXpath = '/html/body/div[1]/main/div/div/div[2]/div/div[14]/div/div[2]/div[1]/div[2]/div['+str(i)+']/div/div/div[1]/div/div/div[6]/div/div/select'
        flag = isElementExist(driver, fullXpath)
        if flag:
            sel = driver.find_element_by_xpath(fullXpath)
            driver.execute_script("arguments[0].scrollIntoView(false)", sel)
            time.sleep(1)
            Select(sel).select_by_value('No refund')

            time.sleep(1)
        i = i + 1

    time.sleep(1)
    btn = driver.find_element_by_xpath('//div[text()="SUBMIT REFUND DECISION"]')
    driver.execute_script("arguments[0].scrollIntoView(false)", btn)
    time.sleep(1)
    btn.click()

    time.sleep(2)
    btn = driver.find_element_by_xpath('//div[text()="CONFIRM REFUND DECISION"]')
    driver.execute_script("arguments[0].scrollIntoView(false)", btn)
    time.sleep(1)
    btn.click()

def eaApproveAllDispute(driver, disputeId):
    time.sleep(5)
    wait = WebDriverWait(driver, 4)
    searchText = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="searchOrders"]')))
    searchText.send_keys(disputeId)
    time.sleep(2)
    driver.find_element_by_xpath(
        '/html/body/div[1]/main/div/div/div[2]/div/div[15]/div/div[2]/table/tbody/tr[1]/td[1]/button').click()

    ReturnCommon.processDispute(driver)


def eaApprovePartiallyDispute(eaSellerDriver, disputeId):
    time.sleep(5)
    wait = WebDriverWait(eaSellerDriver, 4)
    searchText = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="searchOrders"]')))
    searchText.send_keys(disputeId)
    time.sleep(2)
    eaSellerDriver.find_element_by_xpath(
        '/html/body/div[1]/main/div/div/div[2]/div/div[15]/div/div[2]/table/tbody/tr[1]/td[1]/button').click()

    ReturnCommon.processDisputePartially(eaSellerDriver)


def eaRejectDispute(eaSellerDriver, disputeId):
    time.sleep(3)
    wait = WebDriverWait(eaSellerDriver, 4)
    searchText = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="searchOrders"]')))
    searchText.send_keys(disputeId)
    time.sleep(2)
    eaSellerDriver.find_element_by_xpath(
        '/html/body/div[1]/main/div/div/div[2]/div/div[15]/div/div[2]/table/tbody/tr[1]/td[1]/button').click()

    ReturnCommon.sellerRejectDispute(eaSellerDriver)

