import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, wait
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import Select
from selenium import webdriver
import time

def processDispute(driver):
    time.sleep(2)
    wait = WebDriverWait(driver, 4)
    reviewBtn = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//p[text()="Review Request"]')))
    time.sleep(1)
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//p[text()="Review Request"]'))
    time.sleep(1)
    reviewBtn.click()

    time.sleep(5)
    makeBtn = driver.find_element_by_xpath('//p[text()="Make Decision"]')
    time.sleep(1)
    driver.execute_script("arguments[0].scrollIntoView(false)", makeBtn)
    time.sleep(1)
    makeBtn.click()

    time.sleep(2)
    rows = driver.find_elements_by_xpath('/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr')
    rowsNum = len(rows)
    i = 1
    while rowsNum >= i:
        xPath = '/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr[' + str(i) + ']/td[2]/p/div/div/button'
        driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xPath))
        time.sleep(1)
        driver.find_element_by_xpath(xPath).click()
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr[' + str(i) + ']/td[2]/p/div/div/div/div/div/button[1]').click()
        time.sleep(1)
        i = i + 1


    xPath = '/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/label/span[1]/span[1]/input'
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xPath))
    time.sleep(1)
    driver.find_element_by_xpath(xPath).click()
    time.sleep(3)
    submitButton = driver.find_element_by_xpath('//span[text()="Submit"]')
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", submitButton)
    time.sleep(1)
    submitButton.click()
    time.sleep(5)


def processDisputePartially(driver):
    time.sleep(2)
    wait = WebDriverWait(driver, 4)
    reviewBtn = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//p[text()="Review Request"]')))
    time.sleep(1)
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//p[text()="Review Request"]'))
    time.sleep(1)
    reviewBtn.click()

    time.sleep(5)
    makeBtn = driver.find_element_by_xpath('//p[text()="Make Decision"]')
    time.sleep(1)
    driver.execute_script("arguments[0].scrollIntoView(false)", makeBtn)
    time.sleep(1)
    makeBtn.click()

    time.sleep(2)
    rows = driver.find_elements_by_xpath('/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr')
    rowsNum = len(rows)
    i=1
    while rowsNum >= i:
        xPath = '/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr[' + str(i) + ']/td[2]/p/div/div/button'
        driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xPath))
        time.sleep(1)
        driver.find_element_by_xpath(xPath).click()
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr[' + str(i) + ']/td[2]/p/div/div/div/div/div/button[2]').click()
        time.sleep(1)

        inputRefundAmout  = driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr[' + str(i) + ']/td[4]/p/div/div/div/input')
        inputRefundAmout.clear()
        inputRefundAmout.send_keys("1")

        inputReason  = driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr[' + str(i) + ']/td[7]/p/div/div/div/input')
        inputReason.clear()
        inputReason.send_keys("test")
        i = i + 1

    xPath = '/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/label/span[1]/span[1]/input'
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xPath))
    time.sleep(1)
    driver.find_element_by_xpath(xPath).click()
    time.sleep(3)
    submitButton = driver.find_element_by_xpath('//span[text()="Submit"]')
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", submitButton)
    time.sleep(1)
    submitButton.click()
    time.sleep(5)


def sellerRejectDispute(driver):
    time.sleep(2)
    wait = WebDriverWait(driver, 4)
    reviewBtn = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//p[text()="Review Request"]')))
    time.sleep(1)
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//p[text()="Review Request"]'))
    time.sleep(1)
    reviewBtn.click()

    time.sleep(5)
    makeBtn = driver.find_element_by_xpath('//p[text()="Make Decision"]')
    time.sleep(1)
    driver.execute_script("arguments[0].scrollIntoView(false)", makeBtn)
    time.sleep(1)
    makeBtn.click()

    time.sleep(2)
    rows = driver.find_elements_by_xpath('/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr')
    rowsNum = len(rows)
    i=1
    while rowsNum >= i:
        xPath = '/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr[' + str(i) + ']/td[2]/p/div/div/button'
        driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xPath))
        time.sleep(1)
        driver.find_element_by_xpath(xPath).click()
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr[' + str(i) + ']/td[2]/p/div/div/div/div/div/button[3]').click()
        time.sleep(1)

        inputReason  = driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[3]/table/tbody/tr[' + str(i) + ']/td[7]/p/div/div/div/input')
        inputReason.clear()
        inputReason.send_keys("test")
        i = i + 1

    xPath = '/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/label/span[1]/span[1]/input'
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xPath))
    time.sleep(1)
    driver.find_element_by_xpath(xPath).click()
    time.sleep(3)
    submitButton = driver.find_element_by_xpath('//span[text()="Submit"]')
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", submitButton)
    time.sleep(1)
    submitButton.click()
    time.sleep(5)