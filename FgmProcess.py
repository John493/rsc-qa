import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, wait
from selenium.webdriver.support.ui import WebDriverWait

import ReturnCommon

def searchFgmReturn(driver, returnOrderNumber, type):
    time.sleep(2)
    rows = driver.find_elements_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[1]/div[1]/div')
    i = 1
    for row in rows:
        text = driver.find_element_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[1]/div[1]/div['+str(i)+']/button/div/div[1]/p[2]').text
        if text == returnOrderNumber:

            tableRows = driver.find_elements_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[1]/div[1]/div['+str(i)+']/div/div/div')
            tableRowSize = len(tableRows)
            if type == 'return':

                driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[1]/div[1]/div['+str(i)+']/div/div/div['+str(tableRowSize)+']/a/button[1]/div'))
                time.sleep(1)
                driver.find_element_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[1]/div[1]/div['+str(i)+']/div/div/div['+str(tableRowSize)+']/a/button[1]/div').click()
            elif type == 'refund':
                driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[1]/div[1]/div['+str(i)+']/div/div/div['+str(tableRowSize-1)+']/div/button'))
                time.sleep(1)
                driver.find_element_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[1]/div[1]/div['+str(i)+']/div/div/div['+str(tableRowSize-1)+']/div/button').click()
            break
        i = i + 1


def fgmApproveRefund(driver, retrunOrderNumber):
    searchFgmReturn(driver, retrunOrderNumber, 'refund')
    time.sleep(2)
    wait = WebDriverWait(driver, 10)
    submitButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//div[text()="Approve Refund"]')))
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//div[text()="Approve Refund"]'))
    time.sleep(1)
    submitButton.click()

def approveReturn(driver, pdfUrl):
    time.sleep(2)
    rows = driver.find_elements_by_xpath('/html/body/div[1]/div[2]/div[3]/div')
    rowsLength = len(rows)
    i = 2
    while rowsLength >= i :
        driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div['+str(i)+']/div[2]/div[2]/div[1]/div[2]/button'))
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div['+str(i)+']/div[2]/div[2]/div[1]/div[2]/button').click()
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div['+str(i)+']/div[2]/div[2]/div[1]/div[2]/div/div/div/button[1]').click()
        time.sleep(1)
        driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div['+str(i)+']/div[2]/div[2]/div[3]/label/span'))
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div['+str(i)+']/div[2]/div[2]/div[3]/label/span').click()
        time.sleep(1)
        i = i + 1

    #滚动到最下面的按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//span[text()="Next"]'))
    time.sleep(1)
    driver.find_element_by_xpath('//p[text()="Upload Shipping Label*"]').click()
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="fileupload"]').send_keys(pdfUrl)
    time.sleep(10)
    driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/div[2]/div[3]/button[2]/span').click()
    time.sleep(2)
    #滚动到最下面的按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//span[text()="Next"]'))
    time.sleep(1)
    driver.find_element_by_xpath('//span[text()="Next"]').click()
    wait = WebDriverWait(driver, 10)
    submitButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//span[text()="Submit"]')))
    #滚动到最下面的按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//span[text()="Submit"]'))
    time.sleep(1)
    submitButton.click()

    wait = WebDriverWait(driver, 10)
    submitButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//span[text()="Returns & Disputes"]')))
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//span[text()="Returns & Disputes"]'))
    time.sleep(1)
    submitButton.click()


def fgmRejectReturn(driver):
    time.sleep(10)

    rows = driver.find_elements_by_xpath('/html/body/div[1]/div[2]/div[3]/div')
    rowsLength = len(rows)
    i = 2
    while rowsLength >= i :
        if rowsLength == i:
            time.sleep(1)
            driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//span[text()="Next"]'))
            time.sleep(1)
        else:
            driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div['+str(i+1)+']/div[2]/div[2]/div[1]/div[2]/button'))

        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div['+str(i)+']/div[2]/div[2]/div[1]/div[2]/button').click()
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div['+str(i)+']/div[2]/div[2]/div[1]/div[2]/div/div/div/button[3]').click()
        time.sleep(1)
        btnPath2 = '/html/body/div[1]/div[2]/div[3]/div['+str(i)+']/div[2]/div[2]/div[2]/div/div[2]/button'
        time.sleep(1)
        driver.find_element_by_xpath(btnPath2).click()
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div['+str(i)+']/div[2]/div[2]/div[2]/div/div[2]/div/div/div/button[1]').click()
        time.sleep(1)
        i = i + 1

    #滚动到最下面的按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//span[text()="Next"]'))
    time.sleep(1)
    driver.find_element_by_xpath('//span[text()="Next"]').click()

    wait = WebDriverWait(driver, 10)
    submitButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//span[text()="Submit"]')))
    #滚动到最下面的按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//span[text()="Submit"]'))
    time.sleep(1)
    submitButton.click()

def fgmApproveAllDispute(driver, disputeId, pdfUrl):
    rows = driver.find_elements_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[2]/div[1]/div')
    rowsLength = len(rows)
    i = 1
    while rowsLength >= i:
        text = driver.find_element_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[2]/div[1]/div['+str(i)+']/button/div/div/div[1]/p[2]').text
        if text == disputeId:
            xPath = '/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[2]/div[1]/div['+str(i)+']/button/div/a'
            driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xPath))
            time.sleep(1)
            driver.find_element_by_xpath(xPath).click()
            break

        i = i + 1
    time.sleep(3)
    ReturnCommon.processDispute(driver)
    time.sleep(3)

    shippingButton = driver.find_element_by_xpath('//p[text()="Provide Shipping Label"]')
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", shippingButton)
    time.sleep(1)
    shippingButton.click()
    time.sleep(1)

    driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/div[2]/div[1]/label[1]/span[2]').click()
    time.sleep(2)
    driver.find_element_by_xpath('//*[@id="pdfupload"]').send_keys(pdfUrl)
    time.sleep(10)
    submitButton = driver.find_element_by_xpath('//span[text()="Submit"]')
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", submitButton)
    time.sleep(1)
    submitButton.click()
    time.sleep(5)


def fgmApprovePartiallyDispute(driver, disputeId, pdfUrl):
    rows = driver.find_elements_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[2]/div[1]/div')
    rowsLength = len(rows)
    i = 1
    while rowsLength >= i:
        text = driver.find_element_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[2]/div[1]/div['+str(i)+']/button/div/div/div[1]/p[2]').text
        if text == disputeId:
            xPath = '/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[2]/div[1]/div['+str(i)+']/button/div/a'
            driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xPath))
            time.sleep(1)
            driver.find_element_by_xpath(xPath).click()
            break

        i = i + 1
    time.sleep(3)
    ReturnCommon.processDisputePartially(driver)
    time.sleep(3)




def  fgmRejectDispute(driver, disputeId):
    rows = driver.find_elements_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[2]/div[1]/div')

    rowsLength = len(rows)
    i = 1
    while rowsLength >= i:
        text = driver.find_element_by_xpath('/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[2]/div[1]/div['+str(i)+']/button/div/div/div[1]/p[2]').text
        if text == disputeId:
            xPath = '/html/body/div[1]/main/div/div/div[2]/div/div[13]/div/div/div[2]/div[2]/div[1]/div['+str(i)+']/button/div/a'
            driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xPath))
            time.sleep(1)
            driver.find_element_by_xpath(xPath).click()
            break

        i = i + 1
    time.sleep(3)
    ReturnCommon.sellerRejectDispute(driver)
    time.sleep(3)

