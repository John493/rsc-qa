import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, wait
from selenium.webdriver.support.ui import WebDriverWait

def searchOrder(driver, parentOrderNumber):
    time.sleep(20)
    searchText = driver.find_element_by_xpath('//*[@id="searchOrders"]')
    searchText.send_keys(parentOrderNumber)
    searchText.send_keys(Keys.RETURN)
    time.sleep(2)
    driver.find_element_by_xpath(
        '/html/body/div[1]/main/div[2]/div[1]/div[2]/div[8]/div/div[3]/button').send_keys(Keys.RETURN)

def createdReturn(driver):
    time.sleep(3)
    driver.find_element_by_xpath('//div[text()="Return"]').click()
    time.sleep(2)
    checkBoxs = driver.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div')
    i = 3
    checkBoxLength = len(checkBoxs)
    while checkBoxLength >= i:
        disabled = driver.find_element_by_xpath('//*[@id="root"]/div/div[2]/div[' + str(i) + ']/div/div[1]/div/div/div[1]/div/span/span[1]/input').get_attribute('disabled')
        if (disabled!='true') :
            driver.execute_script('arguments[0].scrollIntoView(false)', driver.find_element_by_xpath('//*[@id="root"]/div/div[2]/div[' + str(i) + ']/div/div[1]/div/div/div[1]/div/span/span[1]/input'))
            time.sleep(1)
            driver.find_element_by_xpath('//*[@id="root"]/div/div[2]/div[' + str(i) + ']/div/div[1]/div/div/div[1]/div/span/span[1]/input').click()
            inputBoxs = driver.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div[' + str(i) + ']/div/div[2]/div/div/div/div')
            j = 1
            for inputbox in inputBoxs:
                disabled = driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[' + str(i) + ']/div/div[2]/div/div/div/div[' + str(j) + ']/div/div[1]/div[1]/div/label/span[1]/span[1]/input').get_attribute('disabled')
                if (disabled!='true') :
                    driver.execute_script('arguments[0].scrollIntoView(false)', driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[' + str(i) + ']/div/div[2]/div/div/div/hr['+ str(j) + ']'))
                    time.sleep(1)
                    qty = int(driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[' + str(i) + ']/div/div[2]/div/div/div/div[' + str(j) + ']/div/div/div[1]/div/div[2]/div[1]/input').get_attribute('value'))
                    while qty >0:
                        driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[' + str(i) + ']/div/div[2]/div/div/div/div[' + str(j) + ']/div/div/div[1]/div/div[2]/div[1]/button[1]').click()
                        qty = qty - 1

                    driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[' + str(i) + ']/div/div[2]/div/div/div/div[' + str(j) + ']/div/div/div[1]/div/div[2]/div[1]/button[2]').click()

                j = j+1
        time.sleep(2)
        i = i+1
    #滚动到最下面的按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//*[@id="root"]/div/div[3]/button[2]'))
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="root"]/div/div[3]/button[2]').click()
    time.sleep(3)
    driver.refresh

    selectBtnList = driver.find_elements_by_xpath('/html/body/div[1]/div/div[2]/div')
    i = 3
    selectBtnListLength = len(selectBtnList)
    print(selectBtnListLength)
    # js = "var q = document.documentElement.scrollTop=0"
    # driver.execute_script(js)
    while selectBtnListLength >= i:
        selectList = driver.find_elements_by_xpath('/html/body/div[1]/div/div[2]/div[' + str(i) + ']/div/div[2]/div/div/div/div')
        j = 1
        for select in selectList:
            driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[' + str(i) + ']/div/div[2]/div/div/div/div[' + str(j) + ']/div/div/div[2]/div[1]/div/div/button'))
            time.sleep(1)
            driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[' + str(i) + ']/div/div[2]/div/div/div/div[' + str(j) + ']/div/div/div[2]/div[1]/div/div/button').click()
            time.sleep(1)
            optionList = driver.find_elements_by_xpath('/html/body/div[1]/div/div[2]/div[' + str(i) + ']/div/div[2]/div/div/div/div[' + str(j) + ']/div/div/div[2]/div[1]/div/div/div/div/button')
            for option in optionList:
                if ("Did Not Receive This Item" == option.text):
                    option.click()
            j = j+1

        time.sleep(1)
        i = i+1
    time.sleep(1)
    #滚动到最下面的按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//span[text()="Next"]'))
    wait = WebDriverWait(driver, 4)
    nextBtn = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//span[text()="Next"]')))
    time.sleep(1)
    nextBtn.click()
    time.sleep(5)
    wait = WebDriverWait(driver, 4)
    submitBtn = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//span[text()="Submit"]')))
    submitBtn.click()
    time.sleep(10)
    resultList = driver.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div')
    resultListLength = len(resultList)
    index = 1
    returnOrderNumberList = []

    while resultListLength >= index :
        text = driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div['+str(index)+']/div[1]/h3').text
        returnOrderNumberList.append(text.split(':')[1].strip())
        index = index + 1
    return returnOrderNumberList


def cancelReturn(driver):
    time.sleep(2)
    driver.find_element_by_xpath('//span[text()="Cancel Return Request"]').click()
    time.sleep(3)
    driver.find_element_by_xpath('//span[text()="Confirm"]').click()

def searchReturnOrder(driver, returnOrderNumber):
    time.sleep(5)
    wait = WebDriverWait(driver, 4)
    searchText = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="searchOrders"]')))
    searchText.send_keys(returnOrderNumber)
    searchText.send_keys(Keys.RETURN)
    time.sleep(2)
    driver.find_element_by_xpath(
        '/html/body/div[1]/main/div[2]/div[1]/div[2]/div[13]/div/div[2]/div[2]/button').send_keys(Keys.RETURN)


def createDispute(driver):
    #滚动到按钮
    btnPath = '//div[text()="Submit Dispute"]'
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(btnPath))
    time.sleep(1)
    driver.find_element_by_xpath(btnPath).click()
    time.sleep(1)

    driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[4]/div/div[1]/div/div[1]/div/div/div[1]/span/span[1]/input').click()
    rows = driver.find_elements_by_xpath('/html/body/div[1]/div/div[2]/div[4]/div/div[1]/div/div[2]/div/div/div/div')
    rowsNum = len(rows)
    i = 2
    while rowsNum >= i:
        disabled = driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[4]/div/div[1]/div/div[2]/div/div/div/div[' + str(i) + ']/span').get_attribute('aria-disabled')
        if (disabled!='true') :
            driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[4]/div/div[1]/div/div[2]/div/div/div/div[' + str(i) + ']/div/div/div[2]/div[3]/div'))
            time.sleep(1)
            driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[4]/div/div[1]/div/div[2]/div/div/div/div[' + str(i) + ']/div/div/div[2]/div[1]/div[2]/button').click()
            time.sleep(1)
            driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[4]/div/div[1]/div/div[2]/div/div/div/div[' + str(i) + ']/div/div/div[2]/div[1]/div[2]/div/div/div/button[3]').click()
            time.sleep(1)
        i = i + 1

    btnPath = '//span[text()="Next"]'

    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(btnPath))
    time.sleep(1)
    driver.find_element_by_xpath(btnPath).click()
    time.sleep(1)

    wait = WebDriverWait(driver, 10)
    submitButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//span[text()="Submit"]')))
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//span[text()="Submit"]'))
    time.sleep(1)
    submitButton.click()
    time.sleep(5)

    text = driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[3]/div[1]/p[2]').text
    return text.split(':')[1].strip().replace('.', '')

def cancelDispute(driver):
    time.sleep(1)

    wait = WebDriverWait(driver, 10)
    disputeButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//span[text()="View Dispute Details"]')))
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath('//span[text()="View Dispute Details"]'))
    time.sleep(1)
    disputeButton.click()

    time.sleep(5)

    xpath = '//span[text()="Dispute Summary"]'
    disputeSummaryButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, xpath)))
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xpath))
    time.sleep(1)
    disputeSummaryButton.click()

    time.sleep(5)

    xpath = '//span[text()="Cancel Dispute"]'
    cancelDisputeButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, xpath)))
    #滚动到按钮
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xpath))
    time.sleep(1)
    cancelDisputeButton.click()

    time.sleep(3)

    driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/div[2]/div[1]/div[2]/button').click()

    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/button[1]').click()
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/div[2]/label/span[1]/span[1]/input').click()
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/div[2]/div[4]/button[2]').click()
    time.sleep(1)


def rejectDispute(driver):
    time.sleep(1)
    driver.refresh()

    wait = WebDriverWait(driver, 5)
    xpath = '//span[text()="View Dispute Details"]'
    #滚动到按钮
    viewDisputeButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, xpath)))
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(xpath))
    time.sleep(1)
    viewDisputeButton.click()

    wait = WebDriverWait(driver, 5)

    checkXpath = '/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[1]/div/label/span[1]'
    driver.find_element_by_xpath(checkXpath).click()
    time.sleep(1)

    rejectXpath = '//a[text()="Reject Offer"]'
    rejectButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, rejectXpath)))
    driver.execute_script("arguments[0].scrollIntoView(false)", driver.find_element_by_xpath(rejectXpath))
    time.sleep(1)
    rejectButton.click()
    time.sleep(1)


def buyerUpgradeDispute(driver):
    time.sleep(1)
    driver.refresh()

    wait = WebDriverWait(driver, 10)
    time.sleep(1)
    disputeSummaryXPath = '/html/body/div[1]/div/div[2]/div[3]/div[2]/div/div/div/div[2]/div[1]/button'
    driver.find_element_by_xpath(disputeSummaryXPath).click()
    time.sleep(3)
    escalateDisputeXPath = '//span[text()="Escalate Dispute"]'
    driver.find_element_by_xpath(escalateDisputeXPath).click()
    time.sleep(2)

    escalateReasonXPath = '/html/body/div[3]/div[3]/div/div[2]/div[1]/div[2]/textarea'
    reasonText = wait.until(EC.element_to_be_clickable(
        (By.XPATH, escalateReasonXPath)))
    reasonText.send_keys("tst")
    time.sleep(2)

    checkXPath='/html/body/div[3]/div[3]/div/div[2]/label/span[1]/span[1]'

    driver.find_element_by_xpath(checkXPath).click()

    time.sleep(2)
    submitXpth='//span[text()="Yes"]'
    driver.find_element_by_xpath(submitXpth).click()

