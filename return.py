import time
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, wait
from selenium.webdriver.support.ui import WebDriverWait
import BuyerProcess as buyerProcess
import EaProcess as eaProcess
import FgmProcess as fgmProcess
import AdminProcess as adminProcess

def login(driver, user, password):
    wait = WebDriverWait(driver, 10)
    loginButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//div[text()="SIGN IN"]')))
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="email"]').send_keys(user)
    driver.find_element_by_xpath('//*[@id="password"]').send_keys(password)
    time.sleep(1)
    loginButton.click()

def getToken():
    requestParam = {'email':buyerEmail}
    print(requestParam)
    result = requests.post(getTokenUrl, json=requestParam)
    return result.json()['data']['token']

def getChannel(returnOrderNumber, token):
    headers = {'Authorization':token}
    result = requests.get(getChannelUrl+returnOrderNumber, headers=headers)
    return result.json()['data']['channel']

def adminLogin(adminDriver):
    wait = WebDriverWait(adminDriver, 10)
    loginButton = wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//span[text()="Log In"]')))
    time.sleep(1)

    adminEmial = 'john493@michaels.com'
    adminPassword = 'Qq123456'

    adminDriver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/form/div[1]/div/div[2]/input').send_keys(adminEmial)
    time.sleep(1)
    adminDriver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/form/div[2]/div[2]/span/input').send_keys(adminPassword)
    time.sleep(1)
    loginButton.click()
    time.sleep(10)
    adminDriver.get(adminDisputeUrl)


# 1	创建return-取消return-创建return-approve return
# 2	创建return-reject return
# 3	创建return-拒绝return-创建dispute-全退-同意
# 4	创建return-拒绝return-创建dispute-Buyer cancel
# 9935864957243295,jr_tst03_buyer001@snapmail.cc,Rsc123456,rsc_tst03_seller01@snapmail.cc,Rsc123456,rsc_tst03_maker01@snapmail.cc,Rsc123456
# 9483332235944732,tst03_zybuyer@snapmail.cc,Password123,rsc_tst03_seller01@snapmail.cc,Rsc123456,RC_tst03_fgm_seller01@snapmail.cc,Password123
param = input("Please enter parameters (parentOrderNumber, buyerEmail, buyerPassword, eaSellerEmail, eaSellerPassword, fgmSellerEmail, fgmSellerPassword):")
params = param.split(',')
parentOrderNumber = params[0]
buyerEmail = params[1]
buyerPassword = params[2]
eaSellerEmail = params[3]
eaSellerPassword = params[4]
fgmSellerEmail= params[5]
fgmSellerPassword = params[6]


env = 'tst03'
historyUrl = 'https://mik.'+env+'.platform.michaels.com/buyertools/order-history'
getTokenUrl = "https://usr."+env+".platform.michaels.com/api/private/user/sign-in-nocheck"
getChannelUrl = "https://moh."+env+".platform.michaels.com/api/afterSales/return/buyer/single/getByReturnOrderNumber/"
eaSellerReturnUrl = "https://mik."+env+".platform.michaels.com/mp/sellertools/returns"
eaSellerDisputeUrl = "https://mik."+env+".platform.michaels.com/mp/sellertools/disputes"

fgmSellerReturnUrl = "https://mik."+env+".platform.michaels.com/signin?returnUrl=/fgm/sellertools/returns-disputes?open=returns&page=1"
fgmSellerDisputeUrl = "https://mik."+env+".platform.michaels.com/fgm/sellertools/returns-disputes?open=disputes&page=1"

buyerReturnUrl = 'https://mik.'+env+'.platform.michaels.com/buyertools/return-and-dispute'

adminDisputeUrl = 'https://map.tst03.platform.michaels.com/#/resolution/disputes'

pdfUrl = '/Users/user/1UPS example.pdf'
driverUrl = '/usr/local/bin/chromedriver'
# pdfUrl = 'C:/E/1UPS example.pdf'
# driverUrl = '/Users/Peiqing Qi/Desktop/chromedriver_win32/chromedriver.exe'
token = getToken()
print(token)



def test1():
    driver1 = webdriver.Chrome(driverUrl)
    driver1.maximize_window()
    driver1.get(historyUrl)
    driver1.implicitly_wait(30)  # seconds
    time.sleep(3)
    login(driver1, buyerEmail, buyerPassword)
    buyerProcess.searchOrder(driver1, parentOrderNumber)
    returnOrderNumberList = buyerProcess.createdReturn(driver1)
    print(returnOrderNumberList)
    buyerProcess.cancelReturn(driver1)

    time.sleep(10)
    driver1.get(historyUrl)
    buyerProcess.searchOrder(driver1, parentOrderNumber)
    returnOrderNumberList = buyerProcess.createdReturn(driver1)
    print(returnOrderNumberList)

    #seller process
    for returnOrderNumber in returnOrderNumberList:
        channel = str(getChannel(returnOrderNumber, token))
        print("channel===="+channel)

        if channel == '2':
            eaSellerDriver = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
            eaSellerDriver.maximize_window()
            eaSellerDriver.get(eaSellerReturnUrl)
            eaSellerDriver.implicitly_wait(30)  # seconds
            time.sleep(3)
            login(eaSellerDriver, eaSellerEmail, eaSellerPassword)

            eaProcess.eaSearchOrder(eaSellerDriver, returnOrderNumber)
            eaProcess.eaApproveRefund(eaSellerDriver)
            time.sleep(3)
            returnTextList = eaSellerDriver.find_elements_by_xpath('//p[text()="Returned"]')
            assert len(returnTextList) > 0
            time.sleep(3)
            eaSellerDriver.quit()
        elif channel =='3':
            fgmSellerDriver = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
            fgmSellerDriver.maximize_window()
            fgmSellerDriver.get(fgmSellerReturnUrl)
            fgmSellerDriver.implicitly_wait(30)  # seconds
            time.sleep(3)
            login(fgmSellerDriver, fgmSellerEmail, fgmSellerPassword)

            fgmProcess.searchFgmReturn(fgmSellerDriver, returnOrderNumber, 'return')

            fgmProcess.approveReturn(fgmSellerDriver, pdfUrl)

            fgmProcess.fgmApproveRefund(fgmSellerDriver, returnOrderNumber)
            time.sleep(3)
            assert 1==1
            fgmSellerDriver.quit()
        else:
            print("Current channels are not supported: "+channel)
    print("用例1成功！")
    driver1.quit()

try:
    test1()
except:
    print("用例1错误！")


def test2():
    driver2 = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
    driver2.maximize_window()
    driver2.get(historyUrl)
    driver2.implicitly_wait(30)  # seconds
    login(driver2, buyerEmail, buyerPassword)
    buyerProcess.searchOrder(driver2, parentOrderNumber)
    time.sleep(1)
    returnOrderNumberList = buyerProcess.createdReturn(driver2)
    print(returnOrderNumberList)

    for returnOrderNumber in returnOrderNumberList:
        channel = str(getChannel(returnOrderNumber, token))
        print("channel===="+channel)

        if channel == '2':
            eaSellerDriver = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
            eaSellerDriver.maximize_window()
            eaSellerDriver.get(eaSellerReturnUrl)
            eaSellerDriver.implicitly_wait(30)  # seconds
            time.sleep(3)
            login(eaSellerDriver, eaSellerEmail, eaSellerPassword)

            eaProcess.eaSearchOrder(eaSellerDriver, returnOrderNumber)

            eaProcess.eaRejectRefund(eaSellerDriver)
            time.sleep(3)
            assert 1==1
            eaSellerDriver.quit()
        elif channel =='3':
            fgmSellerDriver = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
            fgmSellerDriver.maximize_window()
            fgmSellerDriver.get(fgmSellerReturnUrl)
            fgmSellerDriver.implicitly_wait(30)  # seconds
            time.sleep(3)
            login(fgmSellerDriver, fgmSellerEmail, fgmSellerPassword)

            fgmProcess.searchFgmReturn(fgmSellerDriver, returnOrderNumber, 'return')

            fgmProcess.fgmRejectReturn(fgmSellerDriver)

            time.sleep(3)
            assert 1==1
            fgmSellerDriver.quit()
        else:
            print("Current channels are not supported: "+channel)
    time.sleep(3)
    assert 1==1
    print("用例2成功！")
    driver2.quit()

try:
    test2()
except:
    print("用例2错误！")


def test3():
    driver3 = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
    driver3.maximize_window()
    driver3.get(historyUrl)
    driver3.implicitly_wait(30)  # seconds
    login(driver3, buyerEmail, buyerPassword)
    buyerProcess.searchOrder(driver3, parentOrderNumber)
    time.sleep(1)
    returnOrderNumberList = buyerProcess.createdReturn(driver3)
    print(returnOrderNumberList)
    for returnOrderNumber in returnOrderNumberList:
        channel = str(getChannel(returnOrderNumber, token))
        print("channel===="+channel)

        if channel == '2':
            eaSellerDriver = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
            eaSellerDriver.maximize_window()
            eaSellerDriver.get(eaSellerReturnUrl)
            eaSellerDriver.implicitly_wait(30)  # seconds
            time.sleep(3)
            login(eaSellerDriver, eaSellerEmail, eaSellerPassword)

            eaProcess.eaSearchOrder(eaSellerDriver, returnOrderNumber)

            eaProcess.eaRejectRefund(eaSellerDriver)

            driver3.get(buyerReturnUrl)
            driver3.implicitly_wait(30)  # seconds
            buyerProcess.searchReturnOrder(driver3, returnOrderNumber)

            disputeId = buyerProcess.createDispute(driver3)
            print("disputeId=="+disputeId)
            time.sleep(3)
            eaSellerDriver.get(eaSellerDisputeUrl)
            time.sleep(3)
            eaProcess.eaApproveAllDispute(eaSellerDriver, disputeId)

            time.sleep(3)
            assert 1==1
            eaSellerDriver.quit()
        elif channel =='3':
            fgmSellerDriver = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
            fgmSellerDriver.maximize_window()
            fgmSellerDriver.get(fgmSellerReturnUrl)
            fgmSellerDriver.implicitly_wait(30)  # seconds
            time.sleep(3)
            login(fgmSellerDriver, fgmSellerEmail, fgmSellerPassword)

            fgmProcess.searchFgmReturn(fgmSellerDriver, returnOrderNumber, 'return')

            fgmProcess.fgmRejectReturn(fgmSellerDriver)

            driver3.get(buyerReturnUrl)
            driver3.implicitly_wait(30)  # seconds
            buyerProcess.searchReturnOrder(driver3, returnOrderNumber)

            disputeId = buyerProcess.createDispute(driver3)
            print("disputeId=="+disputeId)
            time.sleep(3)
            fgmSellerDriver.get(fgmSellerDisputeUrl)
            time.sleep(3)
            fgmProcess.fgmApproveAllDispute(fgmSellerDriver, disputeId, pdfUrl)

            time.sleep(3)
            assert 1==1
            fgmSellerDriver.quit()
        else:
            print("Current channels are not supported: "+channel)

    time.sleep(3)
    assert 1==1
    print("用例3成功！")
    driver3.quit()

try:
    test3()
except:
    print("用例3错误！")


def test4():
    driver4 = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
    driver4.maximize_window()
    driver4.get(historyUrl)
    driver4.implicitly_wait(30)  # seconds
    login(driver4, buyerEmail, buyerPassword)
    buyerProcess.searchOrder(driver4, parentOrderNumber)
    time.sleep(1)
    returnOrderNumberList = buyerProcess.createdReturn(driver4)
    print(returnOrderNumberList)
    for returnOrderNumber in returnOrderNumberList:
        channel = str(getChannel(returnOrderNumber, token))
        print("channel===="+channel)

        if channel == '2':
            eaSellerDriver = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
            eaSellerDriver.maximize_window()
            eaSellerDriver.get(eaSellerReturnUrl)
            eaSellerDriver.implicitly_wait(30)  # seconds
            time.sleep(3)
            login(eaSellerDriver, eaSellerEmail, eaSellerPassword)

            eaProcess.eaSearchOrder(eaSellerDriver, returnOrderNumber)

            eaProcess.eaRejectRefund(eaSellerDriver)

            time.sleep(3)
            assert 1==1
            eaSellerDriver.quit()

        elif channel =='3':
            fgmSellerDriver = webdriver.Chrome('/usr/local/bin/chromedriver')  # Chrome浏览器
            fgmSellerDriver.maximize_window()
            fgmSellerDriver.get(fgmSellerReturnUrl)
            fgmSellerDriver.implicitly_wait(30)  # seconds
            time.sleep(3)
            login(fgmSellerDriver, fgmSellerEmail, fgmSellerPassword)

            fgmProcess.searchFgmReturn(fgmSellerDriver, returnOrderNumber, 'return')

            fgmProcess.fgmRejectReturn(fgmSellerDriver)

            time.sleep(3)
            assert 1==1
            fgmSellerDriver.quit()

        else:
            print("Current channels are not supported: "+channel)

        driver4.get(buyerReturnUrl)
        driver4.implicitly_wait(30)  # seconds
        buyerProcess.searchReturnOrder(driver4, returnOrderNumber)

        disputeId = buyerProcess.createDispute(driver4)

        print("disputeId=="+disputeId)
        buyerProcess.cancelDispute(driver4)

    time.sleep(3)
    assert 1==1
    print("用例4成功！")
    driver4.quit()

try:
    test4()
except:
    print("用例4错误！")





def return13():
    driver13 = webdriver.Chrome(driverUrl)  # Chrome浏览器
    driver13.maximize_window()
    driver13.get(historyUrl)
    driver13.implicitly_wait(30)  # seconds
    login(driver13, buyerEmail, buyerPassword)
    buyerProcess.searchOrder(driver13, parentOrderNumber)
    time.sleep(1)
    returnOrderNumberList = buyerProcess.createdReturn(driver13)
    print(returnOrderNumberList)
    for returnOrderNumber in returnOrderNumberList:
        channel = str(getChannel(returnOrderNumber, token))
        if channel == '2':
            eaSellerDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
            eaSellerDriver.maximize_window()
            eaSellerDriver.get(eaSellerReturnUrl)
            eaSellerDriver.implicitly_wait(15)  # seconds
            time.sleep(3)
            login(eaSellerDriver, eaSellerEmail, eaSellerPassword)

            eaProcess.eaSearchOrder(eaSellerDriver, returnOrderNumber)

            eaProcess.eaRejectRefund(eaSellerDriver)

            # buyer create dispute
            driver13.get(buyerReturnUrl)
            driver13.implicitly_wait(15)  # seconds
            buyerProcess.searchReturnOrder(driver13, returnOrderNumber)
            disputeId = buyerProcess.createDispute(driver13)

            print("disputeId=="+disputeId)
            #seller partially refund amout
            time.sleep(3)
            eaSellerDriver.get(eaSellerDisputeUrl)
            time.sleep(3)
            eaProcess.eaApprovePartiallyDispute(eaSellerDriver, disputeId)


            #buyer reject dispute
            print("disputeId=="+disputeId)
            buyerProcess.rejectDispute(driver13)
            #buyer 升级 dispute
            buyerProcess.buyerUpgradeDispute(driver13)


        elif channel == '3':
            fgmSellerDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
            fgmSellerDriver.maximize_window()
            fgmSellerDriver.get(fgmSellerReturnUrl)
            fgmSellerDriver.implicitly_wait(15)  # seconds
            time.sleep(3)
            login(fgmSellerDriver, fgmSellerEmail, fgmSellerPassword)

            #reject return
            fgmProcess.searchFgmReturn(fgmSellerDriver, returnOrderNumber, 'return')

            fgmProcess.fgmRejectReturn(fgmSellerDriver)

            # buyer create dispute
            driver13.get(buyerReturnUrl)
            driver13.implicitly_wait(15)  # seconds
            buyerProcess.searchReturnOrder(driver13, returnOrderNumber)
            disputeId = buyerProcess.createDispute(driver13)
            print("disputeId=="+disputeId)
            #seller reject
            time.sleep(3)
            fgmSellerDriver.get(fgmSellerDisputeUrl)
            time.sleep(10)
            fgmProcess.fgmApprovePartiallyDispute(fgmSellerDriver, disputeId, pdfUrl)

            #buyer reject dispute
            print("disputeId=="+disputeId)
            buyerProcess.rejectDispute(driver13)
            #buyer 升级 dispute
            buyerProcess.buyerUpgradeDispute(driver13)

        else:
            print("Current channels are not supported: "+channel)

        #admin reject
        adminDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
        adminDriver.maximize_window()
        adminDriver.get(adminDisputeUrl)
        adminDriver.implicitly_wait(15)  # seconds
        time.sleep(3)

        adminLogin(adminDriver)
        #admin search
        adminProcess.adminSearchOrder(adminDriver,disputeId)

        #admin reject dispute
        adminProcess.adminRejectDispute(adminDriver)

        assert 1==1
        print("用例13成功！")
        driver13.quit()



def return14():
    driver14 = webdriver.Chrome(driverUrl)  # Chrome浏览器
    driver14.maximize_window()
    driver14.get(historyUrl)
    driver14.implicitly_wait(30)  # seconds
    login(driver14, buyerEmail, buyerPassword)
    buyerProcess.searchOrder(driver14, parentOrderNumber)
    time.sleep(1)
    returnOrderNumberList = buyerProcess.createdReturn(driver14)
    print(returnOrderNumberList)
    for returnOrderNumber in returnOrderNumberList:
        channel = str(getChannel(returnOrderNumber, token))
        if channel == '2':
            eaSellerDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
            eaSellerDriver.maximize_window()
            eaSellerDriver.get(eaSellerReturnUrl)
            eaSellerDriver.implicitly_wait(15)  # seconds
            time.sleep(3)
            login(eaSellerDriver, eaSellerEmail, eaSellerPassword)

            #reject return
            eaProcess.eaSearchOrder(eaSellerDriver, returnOrderNumber)
            eaProcess.eaRejectRefund(eaSellerDriver)

            # buyer create dispute
            driver14.get(buyerReturnUrl)
            driver14.implicitly_wait(15)  # seconds
            buyerProcess.searchReturnOrder(driver14, returnOrderNumber)
            disputeId = buyerProcess.createDispute(driver14)

            #seller reject
            print("disputeId=="+disputeId)
            time.sleep(3)
            eaSellerDriver.get(eaSellerDisputeUrl)
            time.sleep(3)
            eaProcess.eaRejectDispute(eaSellerDriver, disputeId)


            #buyer reject dispute
            print("disputeId=="+disputeId)
            buyerProcess.rejectDispute(driver14)
            #buyer 升级 dispute
            buyerProcess.buyerUpgradeDispute(driver14)


        elif channel == '3':
            fgmSellerDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
            fgmSellerDriver.maximize_window()
            fgmSellerDriver.get(fgmSellerReturnUrl)
            fgmSellerDriver.implicitly_wait(15)  # seconds
            time.sleep(3)
            login(fgmSellerDriver, fgmSellerEmail, fgmSellerPassword)

            #seller reject return
            fgmProcess.searchFgmReturn(fgmSellerDriver, returnOrderNumber, 'return')
            fgmProcess.fgmRejectReturn(fgmSellerDriver)

            # buyer create dispute
            driver14.get(buyerReturnUrl)
            driver14.implicitly_wait(30)  # seconds
            buyerProcess.searchReturnOrder(driver14, returnOrderNumber)
            disputeId = buyerProcess.createDispute(driver14)

            #seller reject
            fgmProcess.fgmRejectDispute(fgmSellerDriver, disputeId)

            #buyer reject dispute
            print("disputeId=="+disputeId)
            buyerProcess.rejectDispute(driver14)
            #buyer 升级 dispute
            buyerProcess.buyerUpgradeDispute(driver14)


        else:
            print("Current channels are not supported: "+channel)

        #admin 全退
        adminDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
        adminDriver.maximize_window()
        adminDriver.get(adminDisputeUrl)
        adminDriver.implicitly_wait(15)  # seconds
        time.sleep(3)

        adminLogin(adminDriver)
        #admin search
        adminProcess.adminSearchOrder(adminDriver,disputeId)

        adminProcess.adminAllRefund(adminDriver)

        assert 1==1
        print("用例14成功！")
        driver14.quit()

def return15():
    driver15 = webdriver.Chrome(driverUrl)  # Chrome浏览器
    driver15.maximize_window()
    driver15.get(historyUrl)
    driver15.implicitly_wait(30)  # seconds
    login(driver15, buyerEmail, buyerPassword)
    buyerProcess.searchOrder(driver15, parentOrderNumber)
    time.sleep(1)
    returnOrderNumberList = buyerProcess.createdReturn(driver15)
    print(returnOrderNumberList)
    for returnOrderNumber in returnOrderNumberList:
        channel = str(getChannel(returnOrderNumber, token))
        if channel == '2':
            eaSellerDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
            eaSellerDriver.maximize_window()
            eaSellerDriver.get(eaSellerReturnUrl)
            eaSellerDriver.implicitly_wait(15)  # seconds
            time.sleep(3)
            login(eaSellerDriver, eaSellerEmail, eaSellerPassword)

            #seller reject return
            eaProcess.eaSearchOrder(eaSellerDriver, returnOrderNumber)
            eaProcess.eaRejectRefund(eaSellerDriver)

            # buyer create dispute
            driver15.get(buyerReturnUrl)
            driver15.implicitly_wait(30)  # seconds
            buyerProcess.searchReturnOrder(driver15, returnOrderNumber)
            disputeId = buyerProcess.createDispute(driver15)

            #seller reject
            print("disputeId=="+disputeId)
            time.sleep(3)
            eaSellerDriver.get(eaSellerDisputeUrl)
            time.sleep(3)
            eaProcess.eaRejectDispute(eaSellerDriver, disputeId)


            #buyer reject dispute
            print("disputeId=="+disputeId)
            buyerProcess.rejectDispute(driver15)
            #buyer 升级 dispute
            buyerProcess.buyerUpgradeDispute(driver15)




        elif channel == '3':
            fgmSellerDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
            fgmSellerDriver.maximize_window()
            fgmSellerDriver.get(fgmSellerReturnUrl)
            fgmSellerDriver.implicitly_wait(15)  # seconds
            time.sleep(3)
            login(fgmSellerDriver, fgmSellerEmail, fgmSellerPassword)

            #seller reject return
            fgmProcess.searchFgmReturn(fgmSellerDriver, returnOrderNumber, 'return')
            fgmProcess.fgmRejectReturn(fgmSellerDriver)

            # buyer create dispute
            driver15.get(buyerReturnUrl)
            driver15.implicitly_wait(30)  # seconds
            buyerProcess.searchReturnOrder(driver15, returnOrderNumber)
            disputeId = buyerProcess.createDispute(driver15)

            #seller reject
            fgmProcess.fgmRejectDispute(fgmSellerDriver, disputeId)

            #buyer reject dispute
            print("disputeId=="+disputeId)
            buyerProcess.rejectDispute(driver15)
            #buyer 升级 dispute
            buyerProcess.buyerUpgradeDispute(driver15)



        else:
            print("Current channels are not supported: "+channel)

        #admin 部分退
        adminDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
        adminDriver.maximize_window()
        adminDriver.get(adminDisputeUrl)
        adminDriver.implicitly_wait(15)  # seconds
        time.sleep(3)

        adminLogin(adminDriver)
        #admin search
        adminProcess.adminSearchOrder(adminDriver,disputeId)

        adminProcess.adminPartiallyRefund(adminDriver)

        assert 1==1
        print("用例15成功！")
        driver15.quit()

def return16():
    driver16 = webdriver.Chrome(driverUrl)  # Chrome浏览器
    driver16.maximize_window()
    driver16.get(historyUrl)
    driver16.implicitly_wait(30)  # seconds
    login(driver16, buyerEmail, buyerPassword)
    buyerProcess.searchOrder(driver16, parentOrderNumber)
    time.sleep(1)
    returnOrderNumberList = buyerProcess.createdReturn(driver16)
    print(returnOrderNumberList)
    for returnOrderNumber in returnOrderNumberList:
        channel = str(getChannel(returnOrderNumber, token))
        if channel == '2':
            eaSellerDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
            eaSellerDriver.maximize_window()
            eaSellerDriver.get(eaSellerReturnUrl)
            eaSellerDriver.implicitly_wait(15)  # seconds
            time.sleep(3)
            login(eaSellerDriver, eaSellerEmail, eaSellerPassword)

            #reject return
            eaProcess.eaSearchOrder(eaSellerDriver, returnOrderNumber)
            eaProcess.eaRejectRefund(eaSellerDriver)

            # buyer create dispute
            driver16.get(buyerReturnUrl)
            driver16.implicitly_wait(30)  # seconds
            buyerProcess.searchReturnOrder(driver16, returnOrderNumber)
            disputeId = buyerProcess.createDispute(driver16)

            #seller partially refund amout
            print("disputeId=="+disputeId)
            time.sleep(3)
            eaSellerDriver.get(eaSellerDisputeUrl)
            time.sleep(3)
            eaProcess.eaRejectDispute(eaSellerDriver, disputeId)


            #buyer reject dispute
            print("disputeId=="+disputeId)
            buyerProcess.rejectDispute(driver16)
            #buyer 升级 dispute
            buyerProcess.buyerUpgradeDispute(driver16)


            #admin reject
        elif channel == '3':
            fgmSellerDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
            fgmSellerDriver.maximize_window()
            fgmSellerDriver.get(fgmSellerReturnUrl)
            fgmSellerDriver.implicitly_wait(15)  # seconds
            time.sleep(3)
            login(fgmSellerDriver, fgmSellerEmail, fgmSellerPassword)

            #reject return
            fgmProcess.searchFgmReturn(fgmSellerDriver, returnOrderNumber, 'return')
            fgmProcess.fgmRejectReturn(fgmSellerDriver)

            # buyer create dispute
            driver16.get(buyerReturnUrl)
            driver16.implicitly_wait(30)  # seconds
            buyerProcess.searchReturnOrder(driver16, returnOrderNumber)
            disputeId = buyerProcess.createDispute(driver16)

            #seller reject
            fgmProcess.fgmRejectDispute(fgmSellerDriver, disputeId)

            #buyer reject dispute
            print("disputeId=="+disputeId)
            buyerProcess.rejectDispute(driver16)
            #buyer 升级 dispute
            buyerProcess.buyerUpgradeDispute(driver16)
        else:
            print("Current channels are not supported: "+channel)

        #admin reject
        adminDriver = webdriver.Chrome(driverUrl)  # Chrome浏览器
        adminDriver.maximize_window()
        adminDriver.get(adminDisputeUrl)
        adminDriver.implicitly_wait(15)  # seconds
        time.sleep(3)

        adminLogin(adminDriver)
        #admin search
        adminProcess.adminSearchOrder(adminDriver,disputeId)

        adminProcess.adminRejectDispute(adminDriver)

        assert 1==1
        print("用例16成功！")
        driver16.quit()

try:
    return13()
except:
    print("用例13错误！")

try:
    return14()
except:
    print("用例14错误！")

try:
    return15()
except:
    print("用例15错误！")

try:
    return16()
except:
    print("用例16错误！")