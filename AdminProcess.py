import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, wait
from selenium.webdriver.support.ui import WebDriverWait



def adminSearchOrder(driver, disputeId):

    wait = WebDriverWait(driver, 5)

    xPath='/html/body/div[1]/section/div[2]/div/section/main/div/div/span/input'
    searchText = wait.until(EC.element_to_be_clickable(
        (By.XPATH, xPath)))
    searchText.send_keys(disputeId)
    time.sleep(5)
    searchText.send_keys(Keys.RETURN)
    driver.find_element_by_xpath(xPath).send_keys(Keys.RETURN)
    time.sleep(5)
    driver.find_element_by_xpath(
        '/html/body/div[1]/section/div[2]/div/section/main/div/div/div[2]/div/div/div/div/div/div/div[2]/table/tbody/tr[2]/td[1]').click()



def adminAllRefund(adminDriver):
    time.sleep(3)
    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath('//div[text()="Review Escalation"]'))

    elementReview=adminDriver.find_element_by_xpath('//div[text()="Review Escalation"]')
    adminDriver.execute_script("arguments[0].click();", elementReview)
    time.sleep(3)

    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath('//div[text()="Provide Adjudication"]'))
    time.sleep(1)
    element= adminDriver.find_element_by_xpath('//div[text()="Provide Adjudication"]')
    adminDriver.execute_script("arguments[0].click();", element)
    time.sleep(3)


    time.sleep(2)
    rows = adminDriver.find_elements_by_xpath('/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr')

    rowsNum = len(rows)
    i=1
    while rowsNum >= i:
        time.sleep(2)
        XPath='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr[' + str(i) + ']/td[2]/div/div/div'
        adminDriver.find_element_by_xpath(XPath).click()
        time.sleep(2)

        XPath2='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr[' + str(i) + ']/td[2]/div/div/div/span[1]/input';
        time.sleep(1)
        adminDriver.find_element_by_xpath(XPath2).send_keys(Keys.ENTER)
        time.sleep(1)

        inputReason  = adminDriver.find_element_by_xpath('/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr[' + str(i) + ']/td[7]/div/div/input')
        inputReason.clear()
        inputReason.send_keys("test")
        i = i + 1

    time.sleep(1)
    checkXPath='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[2]/div/label/span[1]/span[1]'

    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath(checkXPath))
    adminDriver.find_element_by_xpath(checkXPath).click()
    time.sleep(2)

    submit2XPath='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[3]/button[2]'

    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath(submit2XPath))
    adminDriver.find_element_by_xpath(submit2XPath).click()
    time.sleep(2)


def adminPartiallyRefund(adminDriver):
    time.sleep(3)
    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath('//div[text()="Review Escalation"]'))

    elementReview=adminDriver.find_element_by_xpath('//div[text()="Review Escalation"]')
    adminDriver.execute_script("arguments[0].click();", elementReview)
    time.sleep(3)


    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath('//div[text()="Provide Adjudication"]'))
    element=adminDriver.find_element_by_xpath('//div[text()="Provide Adjudication"]')
    adminDriver.execute_script("arguments[0].click();", element)
    time.sleep(3)

    rows = adminDriver.find_elements_by_xpath('/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr')

    rowsNum = len(rows)
    i=1
    while rowsNum >= i:
        XPath2='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr[' + str(i) + ']/td[2]/div/div/div/span[1]/input';
        adminDriver.find_element_by_xpath(XPath2).send_keys(Keys.DOWN)
        time.sleep(1)
        adminDriver.find_element_by_xpath(XPath2).send_keys(Keys.ENTER)
        time.sleep(1)

        inputRefundAmount  = adminDriver.find_element_by_xpath('/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr[' + str(i) + ']/td[4]/div/div/input')

        inputRefundAmount.clear()
        inputRefundAmount.send_keys("1")

        inputReason  = adminDriver.find_element_by_xpath('/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr[' + str(i) + ']/td[6]/div/div/input')
        inputReason.clear()
        inputReason.send_keys("test")

        i = i + 1

    checkXPath='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[2]/div/label/span[1]/span[1]'

    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath(checkXPath))
    adminDriver.find_element_by_xpath(checkXPath).click()
    time.sleep(3)

    submit2XPath='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[3]/button[2]'

    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath(submit2XPath))
    adminDriver.find_element_by_xpath(submit2XPath).click()
    time.sleep(2)



def  adminRejectDispute(adminDriver):
    time.sleep(5)
    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath('//div[text()="Review Escalation"]'))
    time.sleep(1)
    adminDriver.find_element_by_xpath('//div[text()="Review Escalation"]').click()
    time.sleep(3)

    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath('//div[text()="Provide Adjudication"]'))
    time.sleep(1)
    element= adminDriver.find_element_by_xpath('//div[text()="Provide Adjudication"]')
    adminDriver.execute_script("arguments[0].click();", element)
    time.sleep(3)

    time.sleep(2)
    rows = adminDriver.find_elements_by_xpath('/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr')
    rowsNum = len(rows)
    i=1
    while rowsNum >= i:
        time.sleep(2)
        XPath='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr[' + str(i) + ']/td[2]/div/div/div'
        adminDriver.find_element_by_xpath(XPath).click()
        time.sleep(2)

        XPath2='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr[' + str(i) + ']/td[2]/div/div/div/span[1]/input';
        adminDriver.find_element_by_xpath(XPath2).send_keys(Keys.DOWN)
        time.sleep(1)
        adminDriver.find_element_by_xpath(XPath2).send_keys(Keys.DOWN)
        time.sleep(1)
        adminDriver.find_element_by_xpath(XPath2).send_keys(Keys.ENTER)
        time.sleep(1)
        inputReason  = adminDriver.find_element_by_xpath('/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[1]/div/div/div/div/div[1]/div/table/tbody/tr[' + str(i) + ']/td[7]/div/div/input')
        inputReason.clear()
        inputReason.send_keys("test")
        i = i + 1

    time.sleep(1)
    checkXPath='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[2]/div/label/span[1]/span[1]'

    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath(checkXPath))
    adminDriver.find_element_by_xpath(checkXPath).click()
    time.sleep(2)

    submit2XPath='/html/body/div[1]/section/div[2]/div/section/main/div/div[3]/div/div[3]/button[2]'

    adminDriver.execute_script("arguments[0].scrollIntoView(false)", adminDriver.find_element_by_xpath(submit2XPath))
    adminDriver.find_element_by_xpath(submit2XPath).click()
    time.sleep(2)
